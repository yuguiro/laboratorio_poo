print("***** DATOS DE ENTRADA *****")

nombre = input("TRABAJADOR:")
categoria = input("CATEGORIA:")
extras = input("HORAS EXTRAS:")
tardanza = input("TARDANZAS:")

sueldo_basico = 0
if categoria == 'a' or categoria == 'A':
    sueldo_basico = 3000
if categoria == 'b' or categoria == 'B':
    sueldo_basico = 2500
if  categoria == 'c' or categoria == 'C':
    sueldo_basico = 2000

pago_hora = sueldo_basico/240
pago_minuto = pago_hora/60

hora_extras = 0
if categoria == 'a' or categoria == 'A':
    hora_extras = pago_hora * 4 * float(extras)
if categoria == 'b' or categoria == 'B':
    hora_extras = pago_hora * 3  * float(extras)
if  categoria == 'c' or categoria == 'C':
    hora_extras = pago_hora * 2 * float(extras)

descuento_tardanza = 0
descuento_tardanza = float(tardanza) * pago_minuto

sueldo_neto = 0
sueldo_neto = sueldo_basico + hora_extras - descuento_tardanza

print("")
print("*** BOLETA DE PAGO ***")
print(f"Nombre:{nombre}")
print(f"CATEGORIA:{categoria}")
print(f"SUELDO BASICO:{sueldo_basico}")
print(f"DESCUENTO TARDANZA:{descuento_tardanza}")
print(f"PAGO HORAS EXTRAS:{hora_extras}")
print(f"SUELDO NETO:{sueldo_neto}")